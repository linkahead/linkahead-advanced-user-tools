#!/usr/bin/env python
# encoding: utf-8
#
# Copyright (C) 2019 Henrik tom Wörden
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import os
import unittest

from caosadvancedtools.scifolder import (AnalysisCFood, ExperimentCFood,
                                         PublicationCFood, SimulationCFood)

data_path = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                         "data")


class CFoodTest(unittest.TestCase):
    def test_analysis(self):
        self.assertFalse(AnalysisCFood.match_item("nopath"))
        path = (data_path+"/DataAnalysis/2010_TestProject/"
                "2019-02-03_something/README.md")
        self.assertTrue(AnalysisCFood.match_item(path))
        AnalysisCFood(path)

    def test_experiment(self):
        self.assertFalse(ExperimentCFood.match_item("nopath"))
        path = (data_path+"/ExperimentalData/2010_TestProject/"
                "2019-02-03_something/README.md")
        self.assertTrue(ExperimentCFood.match_item(path))
        ExperimentCFood(path)

    def test_publication(self):
        self.assertFalse(PublicationCFood.match_item("nopath"))
        path = data_path+"/Publications/Posters/2019-02-03_something/README.md"
        self.assertTrue(PublicationCFood.match_item(path))
        PublicationCFood(path)

    def test_simulation(self):
        self.assertFalse(SimulationCFood.match_item("nopath"))
        path = (data_path + "/SimulationData/2010_TestProject/"
                "2019-02-03_something/README.md")
        self.assertTrue(SimulationCFood.match_item(path))
        SimulationCFood(path)
