#!/usr/bin/env python3
# encoding: utf-8
#
# ** header v3.0
# This file is a part of the LinkAhead project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#

"""
test module for ResultTableCFood
"""


import os
import re
import unittest

import linkahead as db
from caosadvancedtools.scifolder.result_table_cfood import ResultTableCFood


class CFoodTest(unittest.TestCase):
    def test_re(self):
        self.assertIsNotNone(re.match(ResultTableCFood.table_re, "result_table_Hallo.csv"))
        self.assertEqual(re.match(ResultTableCFood.table_re, "result_table_Hallo.csv").group("recordtype"),
                         "Hallo")
        self.assertIsNotNone(re.match(ResultTableCFood.table_re,
                                      "result_table_Cool RecordType.csv"))
        self.assertEqual(re.match(ResultTableCFood.table_re, "result_table_Cool RecordType.csv").group("recordtype"),
                         "Cool RecordType")
        self.assertIsNone(re.match(ResultTableCFood.table_re, "result_tableCool RecordType.csv"))

        self.assertIsNotNone(re.match(ResultTableCFood.property_name_re,
                                      "temperature [C]"))
        self.assertEqual(re.match(ResultTableCFood.property_name_re,
                                  "temperature [C]").group("pname"),
                         "temperature")
        self.assertEqual(re.match(ResultTableCFood.property_name_re,
                                  "temperature [C]").group("unit"), "C")
        self.assertEqual(re.match(ResultTableCFood.property_name_re,
                                  "temperature [ C ]").group("unit"), "C")
        self.assertEqual(re.match(ResultTableCFood.property_name_re,
                                  "temperature").group("pname"), "temperature")

    def test_ident(self):
        rtc = ResultTableCFood(os.path.join(os.path.dirname(__file__), "test.csv"))
        rtc.match = re.match(ResultTableCFood.get_re(),
                             "/ExperimentalData/2010_TestProject/2019-02-03_something/result_table_RT.csv")
        rtc.create_identifiables()
        rtc.update_identifiables()
