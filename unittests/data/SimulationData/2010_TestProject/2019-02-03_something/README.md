---
responsible:	
- Only Responsible
description: 	A description of another example experiment.

results:
- file:	"*.dat"
  description:  an example reference to a results file

scripts:
- sim.py
...
