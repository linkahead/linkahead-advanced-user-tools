#!/bin/bash
# execute this script form the root folder of the directory (i.e. ..) for 
# locally testing whtat the pipeline does. 
# Note: this is not 100% replication as the pipeline might change and not every
# thing is replicated. 
CI_REGISTRY=registry.indiscale.com EXEPATH=`pwd` CAOSDB_TAG=dev-latest \
	docker-compose -f .docker/docker-compose.yml pull
CI_REGISTRY_IMAGE=registry.indiscale.com/caosdb-advanced-testenv \
	docker-compose -f .docker/tester.yml pull
CI_REGISTRY=registry.indiscale.com EXEPATH=`pwd` CAOSDB_TAG=dev-latest \
	docker-compose -f .docker/docker-compose.yml up -d
cd .docker
CAOSHOSTNAME=linkahead-server ./cert.sh
CI_REGISTRY_IMAGE=registry.indiscale.com/caosdb-advanced-testenv  ./run.sh
docker-compose  down

