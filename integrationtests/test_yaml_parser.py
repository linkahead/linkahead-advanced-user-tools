# encoding: utf-8
#
# This file is a part of the LinkAhead project.
#
# Copyright (C) 2022 IndiScale GmbH <info@indiscale.com>
# Copyright (C) 2022 Florian Spreckelsen <f.spreckelsen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License along
# with this program. If not, see <https://www.gnu.org/licenses/>.
#

import linkahead as db
from caosadvancedtools.models.parser import parse_model_from_string


def _delete_everything():
    ents = db.execute_query("FIND ENTITY WITH ID > 99")
    if ents:
        ents.delete()


def setup_module():
    _delete_everything()


def teardown_module():
    _delete_everything()


def test_internal_props_in_extern():
    """Test adding the internal `name` property as a parent to an existing
    property.

    """

    model = """
extern:
- name
- test_name
- description
- unit
test_name:
  inherit_from_suggested:
  - name
  - description
  - unit
"""
    db.Property(name="test_name", datatype=db.TEXT).insert()
    ents = parse_model_from_string(model)
    ents.sync_data_model(noquestion=True)

    test_prop = db.Property(name="test_name").retrieve()
    assert len(test_prop.parents) == 3
    desc_prop = db.Property(name="description").retrieve()
    name_prop = db.Property(name="name").retrieve()
    unit_prop = db.Property(name="unit").retrieve()
    assert test_prop.has_parent(desc_prop)
    assert test_prop.has_parent(name_prop)
    assert test_prop.has_parent(unit_prop)
